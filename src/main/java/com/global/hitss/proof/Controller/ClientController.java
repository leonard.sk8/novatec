package com.global.hitss.proof.Controller;

import com.global.hitss.proof.DAO.ClientRepository;
import com.global.hitss.proof.Entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/c/v1")
public class ClientController {

    @Autowired
    private ClientRepository clientRepository;


    @Bean
    public RestTemplate rest() {
        return new RestTemplate();
    }

    @GetMapping(value = "/clients" , produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Client> getAllclients() {
        return new ArrayList<>(clientRepository.findAll());
    }
    @PostMapping("/clients")
    public Client createclient(@Valid @RequestBody Client client) {
        return clientRepository.save(client);
    }
    @PutMapping("/clients/{id}")
    public ResponseEntity<Client> updateclient(
            @PathVariable(value = "id") Integer clientId, @Valid @RequestBody Client clientDetails)
            throws ResourceNotFoundException {
        Client client =
                clientRepository
                        .findById(clientId)
                        .orElseThrow(() -> new ResourceNotFoundException("client not found on :: " + clientId));
        client.setLastNameClient(clientDetails.getLastNameClient());
        client.setNameClient(clientDetails.getNameClient());
        final Client updatedClient = clientRepository.save(client);
        return ResponseEntity.ok(updatedClient);
    }
    @DeleteMapping("/clients/{id}")
    public Map<String, Boolean> deleteClient(@PathVariable(value = "id") Integer clientId) throws Exception {

        Client user =
                clientRepository
                        .findById(clientId)
                        .orElseThrow(() -> new ResourceNotFoundException("Client not found on :: " + clientId));

        clientRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;

    }
}
