package com.global.hitss.proof.Controller;

import com.global.hitss.proof.DAO.BuyRepository;
import com.global.hitss.proof.Entity.Buy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/b/v1")
public class BuyController {

    @Autowired
    private BuyRepository buyRepository;

    @GetMapping("/buys")
    public List<Buy> getAllPurchases() {
        return buyRepository.findAll();
    }
    @PostMapping("/buys")
    public Buy createPurchases(@Valid @RequestBody Buy Buy) {
        return buyRepository.save(Buy);
    }
    @PutMapping("/buys/{id}")
    public ResponseEntity<Buy> updatePurchases(
            @PathVariable(value = "id") Integer productId, @Valid @RequestBody Buy buyDetails)
            throws ResourceNotFoundException {

        Buy purchase =
                buyRepository
                        .findById(productId)
                        .orElseThrow(() -> new ResourceNotFoundException("purchase not found on :: " + productId));
        purchase.setClientId(buyDetails.getClientId());
        purchase.setProductId(buyDetails.getProductId());
        purchase.setDate_buy(buyDetails.getDate_buy());
        final Buy updatedBuy = buyRepository.save(purchase);
        return ResponseEntity.ok(updatedBuy);
    }
    @DeleteMapping("/buys/{id}")
    public Map<String, Boolean> deletePurchases(@PathVariable(value = "id") Integer productId) throws Exception {
        Buy product =
                buyRepository
                        .findById(productId)
                        .orElseThrow(() -> new ResourceNotFoundException("Buy not found on :: " + productId));

        buyRepository.delete(product);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
