package com.global.hitss.proof.Controller;

import com.global.hitss.proof.DAO.PriceRepository;
import com.global.hitss.proof.Entity.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/pr/v1")
public class PriceController {

    @Autowired
    private PriceRepository priceRepository;

    @GetMapping("/prices")
    public List<Price> getAllPrices() {
        return priceRepository.findAll();
    }
    @PostMapping("/prices")
    public Price createPrice(@Valid @RequestBody Price price) {
        return priceRepository.save(price);
    }
    @PutMapping("/prices/{id}")
    public ResponseEntity<Price> updatePrice(
            @PathVariable(value = "id") Integer clientId, @Valid @RequestBody Price priceDetails)
            throws ResourceNotFoundException {
        Price price =
                priceRepository
                        .findById(clientId)
                        .orElseThrow(() -> new ResourceNotFoundException("price not found on :: " + clientId));
        price.setPrice(priceDetails.getPrice());
        final Price updatedPrice = priceRepository.save(price);
        return ResponseEntity.ok(updatedPrice);
    }
    @DeleteMapping("/prices/{id}")
    public Map<String, Boolean> deletePrice(@PathVariable(value = "id") Integer clientId) throws Exception {
        Price price =
                priceRepository
                        .findById(clientId)
                        .orElseThrow(() -> new ResourceNotFoundException("Price not found on :: " + clientId));
        priceRepository.delete(price);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
