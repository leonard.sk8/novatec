package com.global.hitss.proof.Config;

import com.global.hitss.proof.Entity.Buy;
import com.global.hitss.proof.Entity.Client;
import com.global.hitss.proof.Entity.Price;
import com.global.hitss.proof.Entity.Product;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;


@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Client.class,Buy.class,Price.class,Product.class);
        config.useHalAsDefaultJsonMediaType(false);
//        config.disableDefaultExposure();
    }
}