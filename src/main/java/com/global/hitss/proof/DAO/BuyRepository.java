package com.global.hitss.proof.DAO;

import com.global.hitss.proof.Entity.Buy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuyRepository extends JpaRepository<Buy,Integer> {

}
