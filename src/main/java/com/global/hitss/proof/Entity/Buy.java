package com.global.hitss.proof.Entity;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "buy")
@EntityListeners(AuditingEntityListener.class)
public class Buy {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idbuy",nullable = false)
    private int idBuy;
    @Column(name = "client_id_client",nullable = false)
    private int clientId;
    @Column(name = "product_idproduct",nullable = false)
    private int productId;
    @Column(name = "date_buy",nullable = false)
    private Date date_buy;

    public int getIdBuy() {
        return idBuy;
    }

    public void setIdBuy(int idBuy) {
        this.idBuy = idBuy;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public Date getDate_buy() {
        return date_buy;
    }

    public void setDate_buy(Date date_buy) {
        this.date_buy = date_buy;
    }
}
