package com.global.hitss.proof;

import com.global.hitss.proof.Entity.Buy;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProofApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BuyTests {

    @Test
    public void contextLoads() {
    }

    @Autowired
    private TestRestTemplate restTemplate;
    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    @Test
    public void testGetAllBuy() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/buys",
                HttpMethod.GET, entity, String.class);
        Assert.assertNotNull(response.getBody());
    }


    @Test
    public void testGetBuyById() {
        Buy buy = restTemplate.getForObject(getRootUrl() + "/buys/1", Buy.class);
        System.out.println(buy);
        Assert.assertNotNull(buy);
    }

    @Test
    public void testCreateBuy() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Buy buy = new Buy();
        buy.setClientId(1);
        buy.setProductId(1);
        buy.setDate_buy(simpleDateFormat.parse("2019-02-02"));
        ResponseEntity<Buy> postResponse = restTemplate.postForEntity(getRootUrl() + "/buys",
                buy, Buy.class);
        System.out.println(getRootUrl());
        System.out.println(postResponse);
        System.out.println(postResponse.getBody());
        Assert.assertNotNull(postResponse);
        Assert.assertNotNull(postResponse.getBody());
    }

    @Test
    public void testUpdateBuyPost() {
        int id = 1;
        Buy buy = restTemplate.getForObject(getRootUrl() + "/buys/" + id, Buy.class);
        buy.setDate_buy(new Date());
        buy.setProductId(4);
        restTemplate.put(getRootUrl() + "/buys/" + id, buy);
        Buy updatedBuy = restTemplate.getForObject(getRootUrl() + "/buys/" + id, Buy.class);
        Assert.assertNotNull(updatedBuy);
    }

    @Test
    public void testDeleteBuyPost() {
        int id = 1;
        Buy buy = restTemplate.getForObject(getRootUrl() + "/buys/" + id, Buy.class);
        Assert.assertNotNull(buy);
        restTemplate.delete(getRootUrl() + "/buys/" + id);
        try {
            buy = restTemplate.getForObject(getRootUrl() + "/buys/" + id, Buy.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
}
