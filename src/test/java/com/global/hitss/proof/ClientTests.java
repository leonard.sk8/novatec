package com.global.hitss.proof;

import com.global.hitss.proof.Entity.Client;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProofApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClientTests {

    @Test
    public void contextLoads() {
    }

    @Autowired
    private TestRestTemplate restTemplate;
    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    @Test
    public void testGetAllClients() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/clients",
                HttpMethod.GET, entity, String.class);
        Assert.assertNotNull(response.getBody());
    }


    @Test
    public void testGetClientById() {
        Client client = restTemplate.getForObject(getRootUrl() + "/clients/1", Client.class);
        System.out.println(client.getNameClient());
        Assert.assertNotNull(client);
    }

    @Test
    public void testCreateClient() {
        Client client = new Client();
        client.setIdClient(1);
        client.setLastNameClient("last name proof");
        client.setNameClient("name client");
        ResponseEntity<Client> postResponse = restTemplate.postForEntity(getRootUrl() + "/clients",
                client, Client.class);
        Assert.assertNotNull(postResponse);
        Assert.assertNotNull(postResponse.getBody());
    }

    @Test
    public void testUpdateClientPost() {
        int id = 1;
        Client client = restTemplate.getForObject(getRootUrl() + "/clients/" + id, Client.class);
        client.setNameClient("Leonardo");
        client.setLastNameClient("Martinez");
        restTemplate.put(getRootUrl() + "/clients/" + id, client);
        Client updatedClient = restTemplate.getForObject(getRootUrl() + "/clients/" + id, Client.class);
        Assert.assertNotNull(updatedClient);
    }

    @Test
    public void testDeleteClientPost() {
        int id = 1;
        Client client = restTemplate.getForObject(getRootUrl() + "/clients/" + id, Client.class);
        Assert.assertNotNull(client);
        restTemplate.delete(getRootUrl() + "/clients/" + id);
        try {
            client = restTemplate.getForObject(getRootUrl() + "/clients/" + id, Client.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
}
