package com.global.hitss.proof;

import com.global.hitss.proof.Entity.Price;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProofApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PriceTests {

    @Test
    public void contextLoads() {
    }

    @Autowired
    private TestRestTemplate restTemplate;
    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    @Test
    public void testGetAllPrice() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/prices",
                HttpMethod.GET, entity, String.class);
        Assert.assertNotNull(response.getBody());
    }


    @Test
    public void testGetPriceById() {
        Price price = restTemplate.getForObject(getRootUrl() + "/prices/1", Price.class);
        System.out.println(price);
        Assert.assertNotNull(price);
    }

    @Test
    public void testCreatePrice() {
        Price price = new Price();
        price.setPrice(200000D);
        ResponseEntity<Price> postResponse = restTemplate.postForEntity(getRootUrl() + "/prices",
                price, Price.class);
        Assert.assertNotNull(postResponse);
        Assert.assertNotNull(postResponse.getBody());
    }

    @Test
    public void testUpdatePricePost() {
        int id = 2;
        Price price = restTemplate.getForObject(getRootUrl() + "/prices/" + id, Price.class);
        price.setPrice(120000D);
        restTemplate.put(getRootUrl() + "/prices/" + id, price);
        Price updatedPrice = restTemplate.getForObject(getRootUrl() + "/prices/" + id, Price.class);
        Assert.assertNotNull(updatedPrice);
    }

    @Test
    public void testDeletePricePost() {
        int id = 2;
        Price price = restTemplate.getForObject(getRootUrl() + "/prices/" + id, Price.class);
        Assert.assertNotNull(price);
        restTemplate.delete(getRootUrl() + "/prices/" + id);
        try {
            price = restTemplate.getForObject(getRootUrl() + "/prices/" + id, Price.class);
        } catch (final HttpClientErrorException e) {
            Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }
}
