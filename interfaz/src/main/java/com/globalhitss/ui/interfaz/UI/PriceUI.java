package com.globalhitss.ui.interfaz.UI;


import com.globalhitss.ui.interfaz.UI.editor.EditorPrice;
import com.globalhitss.ui.interfaz.VO.PriceVO;
import com.globalhitss.ui.interfaz.service.PriceService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


@Route("crud/prices")
public class PriceUI extends UIGeneral<PriceVO> {

    private PriceService repo;
    private EditorPrice editorPrice;

    @Autowired
    public PriceUI(PriceService repo,EditorPrice editorPrice) {
        this.repo=repo;
        this.editorPrice = editorPrice;
        grid = new Grid<>(PriceVO.class);
        add(actions, grid, editorPrice);
        grid.setHeight("300px");
        grid.setColumns("idPrice", "price");
        grid.getColumnByKey("idPrice").setWidth("50px").setFlexGrow(0);
        editorPrice.setChangeHandler(() -> {
            editorPrice.setVisible(false);
            listContent();
        });
        grid.asSingleSelect().addValueChangeListener(e -> {
            editorPrice.editPrice(e.getValue());
        });
        this.editorPrice.setChangeHandler(()->{
            editorPrice.setVisible(false);
            listContent();
        });
        addNewBtn.addClickListener(e -> editorPrice.editPrice(new PriceVO(0,0D)));
        listContent();
    }

    @Override
    void listContent() {
        List<PriceVO> list = repo.getAllPrices();
        grid.setItems(list);
    }
}
