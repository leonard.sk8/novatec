package com.globalhitss.ui.interfaz.VO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientVO {

    private Integer idClient;
    private String nameClient;
    private String lastNameClient;

    public ClientVO() {
    }

    public ClientVO(String nameClient, String lastNameClient) {
        this.idClient = null;
        this.nameClient = nameClient;
        this.lastNameClient = lastNameClient;
    }

    public ClientVO(Integer idClient, String nameClient, String lastNameClient) {
        this.idClient = idClient;
        this.nameClient = nameClient;
        this.lastNameClient = lastNameClient;
    }

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public String getNameClient() {
        return nameClient;
    }

    public void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    public String getLastNameClient() {
        return lastNameClient;
    }

    public void setLastNameClient(String lastNameClient) {
        this.lastNameClient = lastNameClient;
    }
}
