package com.globalhitss.ui.interfaz.UI;


import com.globalhitss.ui.interfaz.UI.editor.EditorClient;
import com.globalhitss.ui.interfaz.VO.ClientVO;
import com.globalhitss.ui.interfaz.service.ClientService;
import com.globalhitss.ui.interfaz.utils.IDNum;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.ArrayList;
import java.util.List;


@Route("crud/client")
public class ClientUI extends UIGeneral<ClientVO> {

    private ClientService repo;
    private EditorClient editorClient;

    @Autowired
    public ClientUI(ClientService repo, EditorClient editor) {
        this.repo = repo;
        this.editorClient = editor;
        grid = new Grid<>(ClientVO.class);
        add(actions, grid, editorClient);
        grid.setHeight("300px");
        grid.setColumns("idClient", "nameClient", "lastNameClient");
        grid.getColumnByKey("idClient").setWidth("50px").setFlexGrow(0);
        grid.asSingleSelect().addValueChangeListener(e -> {
            editorClient.editCustomer(e.getValue());
        });

        this.editorClient.setChangeHandler(()->{
            editorClient.setVisible(false);
            listContent();
        });
        addNewBtn.addClickListener(
                e -> editorClient.editCustomer(new ClientVO("", "")));

        listContent();
    }

    @Override
    void listContent() {
        List<ClientVO> list = repo.getAllClients();
        grid.setItems(list);
    }

}
