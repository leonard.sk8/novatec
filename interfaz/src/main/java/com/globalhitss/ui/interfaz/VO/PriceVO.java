package com.globalhitss.ui.interfaz.VO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceVO {

    private Integer idPrice;
    private Double price;

    public PriceVO() {
    }

    public PriceVO(Integer idPrice, Double price) {
        this.idPrice = idPrice;
        this.price = price;
    }

    public Integer getIdPrice() {
        return idPrice;
    }

    public void setIdPrice(Integer idPrice) {
        this.idPrice = idPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
