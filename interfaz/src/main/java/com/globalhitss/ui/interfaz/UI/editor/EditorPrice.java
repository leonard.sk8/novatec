package com.globalhitss.ui.interfaz.UI.editor;

import com.globalhitss.ui.interfaz.VO.PriceVO;
import com.globalhitss.ui.interfaz.service.PriceService;
import com.globalhitss.ui.interfaz.utils.IDNum;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToDoubleConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class EditorPrice extends VerticalLayout implements KeyNotifier {

    private PriceService repository;
    private PriceVO priceVOObj;

    TextField price = new TextField("price");

    Button save = new Button("Save", VaadinIcon.CHECK.create());
    Button cancel = new Button("Cancel");
    Button delete = new Button("Delete", VaadinIcon.TRASH.create());
    HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

    Binder<PriceVO> binder = new Binder<>(PriceVO.class);
    private ChangeHandler changeHandler;

    @Autowired
    public EditorPrice(PriceService repository) {
        this.repository=repository;
    add(price, actions);
    binder.forField(price).withConverter(new StringToDoubleConverter("must be converted"))
    .bind(PriceVO::getPrice, PriceVO::setPrice);

    setSpacing(true);
    save.getElement().getThemeList().add("primary");
    delete.getElement().getThemeList().add("error");
    addKeyPressListener(Key.ENTER, e -> save());


    save.addClickListener(e -> save());
    delete.addClickListener(e -> delete());
    cancel.addClickListener(e -> editPrice(null));
    setVisible(false);
    }

    void delete() {
        repository.deletePrice(priceVOObj);
        changeHandler.onChange();
    }

    void save() {
        repository.save(priceVOObj);
        changeHandler.onChange();
    }

    public interface ChangeHandler {
    void onChange();
    }

    public final void editPrice(PriceVO c) {
    if (c == null) {
    setVisible(false);
    return;
    }
    final boolean persisted = c.getIdPrice() != null;
    if (persisted) {
        priceVOObj = repository.findById(c.getIdPrice());
    }else {
        c.setIdPrice(IDNum.generateRandomID());
        priceVOObj = c;
    }
    cancel.setVisible(persisted);


    binder.setBean(priceVOObj);

    setVisible(true);

    price.focus();
    }

        public void setChangeHandler(ChangeHandler h) {
    changeHandler = h;
    }

}