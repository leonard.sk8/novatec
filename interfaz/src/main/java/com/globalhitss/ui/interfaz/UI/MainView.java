package com.globalhitss.ui.interfaz.UI;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ClickableRenderer;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinRequest;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;


@Route("main")
public class MainView extends Div {


    VerticalLayout verticalMain;
    Button productsBtn;
    Button pricesBtn;
    Button costumersBtn;
    Button clientsBtn;


    public MainView() {
        this.verticalMain = new VerticalLayout();
        verticalMain.setAlignItems(FlexComponent.Alignment.CENTER);
        Label titulo = new Label("CRUD Global Hitss-Novatec");
        productsBtn = new Button("Productos");
        pricesBtn = new Button("Precios");
        costumersBtn = new Button("Compras");
        clientsBtn = new Button("Clientes");
        clientsBtn.addClickListener(e->clientsBtn.getUI().ifPresent(ui->ui.navigate("crud/client")));
        productsBtn.addClickListener(e->productsBtn.getUI().ifPresent(ui->ui.navigate("crud/product")));
        pricesBtn.addClickListener(e->pricesBtn.getUI().ifPresent(ui->ui.navigate("crud/prices")));
        costumersBtn.addClickListener(e->costumersBtn.getUI().ifPresent(ui->ui.navigate("crud/buy")));
        verticalMain.add(titulo);
        verticalMain.add(productsBtn);
        verticalMain.add(pricesBtn);
        verticalMain.add(costumersBtn);
        verticalMain.add(clientsBtn);
        add(verticalMain);
    }


    void init() {


    }


}
