package com.globalhitss.ui.interfaz.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globalhitss.ui.interfaz.VO.BuyVO;
import com.globalhitss.ui.interfaz.utils.Route;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Repository
public class BuyService extends SuperService<BuyVO> {

    public List<BuyVO> getAllBuys(){
        ObjectMapper mapper = new ObjectMapper();
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/json"));
        messageConverter.setObjectMapper(mapper);
        String uri = Route.url_backend.getURL()+"/buys";
        ResponseEntity<PagedResources<BuyVO>> responseEntity =
                new RestTemplate(Collections.singletonList(messageConverter))
                        .exchange(
                                uri,
                                HttpMethod.GET,
                                HttpEntity.EMPTY,
                                new ParameterizedTypeReference<PagedResources<BuyVO>>() {}, uri
                        );
        PagedResources<BuyVO> clients = responseEntity.getBody();
        assert clients != null;
        ArrayList<BuyVO> listClients = new ArrayList<>();
        for (BuyVO vo:clients ){
            listClients.add(vo);
        }
        return listClients;
    }

    public void delete(BuyVO buy) {
        setPath("/clients/{id}");
        delete(String.valueOf(buy.getIdBuy()));
    }

    @Override
    public void save(BuyVO obj) {
        final String uri = Route.url_backend.getURL()+"/buys";
        RestTemplate restTemplate = new RestTemplate();
        BuyVO result = restTemplate.postForObject( uri, obj, BuyVO.class);
        System.out.println(result);
    }

    @Override
    public BuyVO findById(Integer idprice) {
        final String uri = Route.url_backend.getURL()+"/buys/{id}";
        Map<String, String> params = new HashMap<>();
        params.put("id", idprice+"");
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(uri, BuyVO.class, params);
    }
}
