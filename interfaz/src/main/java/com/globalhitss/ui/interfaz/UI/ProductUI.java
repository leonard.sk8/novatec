package com.globalhitss.ui.interfaz.UI;


import com.globalhitss.ui.interfaz.UI.editor.EditorProduct;
import com.globalhitss.ui.interfaz.VO.ProductVO;
import com.globalhitss.ui.interfaz.service.ProductService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


@Route("crud/product")
public class ProductUI extends UIGeneral<ProductVO> {

    private ProductService repo;
    private EditorProduct editorProduct;

    @Autowired
    public ProductUI(ProductService repo,EditorProduct editorProduct) {
        this.repo=repo;
        this.editorProduct = editorProduct;
        grid = new Grid<>(ProductVO.class);
        add(actions, grid, editorProduct);
        grid.setHeight("300px");
        grid.setColumns("idProduct", "nameProduct", "detailProduct","priceId");
        grid.getColumnByKey("idProduct").setWidth("50px").setFlexGrow(0);
        grid.asSingleSelect().addValueChangeListener(e -> {
            editorProduct.editProduct(e.getValue());
        });
        this.editorProduct.setChangeHandler(()->{
            editorProduct.setVisible(false);
            listContent();
        });
        addNewBtn.addClickListener(e -> editorProduct.editProduct(new ProductVO("","", 1)));
        listContent();
    }

    @Override
    void listContent() {
        List<ProductVO> list = repo.getAllProducts();
        grid.setItems(list);
    }


}
