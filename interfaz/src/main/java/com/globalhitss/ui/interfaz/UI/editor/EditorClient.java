package com.globalhitss.ui.interfaz.UI.editor;

import com.globalhitss.ui.interfaz.VO.ClientVO;
import com.globalhitss.ui.interfaz.service.ClientService;
import com.globalhitss.ui.interfaz.utils.IDNum;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class EditorClient extends UIEditorGeneral {

    private ClientService repository;

    private ClientVO clientVO;

    TextField identification = new TextField("Identification");
    TextField nameClient = new TextField("First name");
    TextField lastNameClient = new TextField("Last name");


    Button save = new Button("Save", VaadinIcon.CHECK.create());
    Button cancel = new Button("Cancel");
    Button delete = new Button("Delete", VaadinIcon.TRASH.create());
    HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

    Binder<ClientVO> binder = new Binder<>(ClientVO.class);
    private ChangeHandler changeHandler;

    @Autowired
    public EditorClient(ClientService repository) {
    this.repository = repository;
    add(identification,nameClient, lastNameClient, actions);
    binder.bindInstanceFields(this);
    setSpacing(true);
    save.getElement().getThemeList().add("primary");
    delete.getElement().getThemeList().add("error");
    addKeyPressListener(Key.ENTER, e -> save());

    save.addClickListener(e -> save());
    delete.addClickListener(e -> delete());
    cancel.addClickListener(e -> editCustomer(null));
    setVisible(false);
    }

    void delete() {
        repository.delete(clientVO);
        changeHandler.onChange();
    }

    void save() {
        repository.save(clientVO);
        changeHandler.onChange();
    }

    public interface ChangeHandler {
    void onChange();
    }

    public final void editCustomer(ClientVO c) {
    if (c == null) {
    setVisible(false);
    return;
    }
    final boolean persisted = c.getIdClient() != null;
    if (persisted) {
//    clientVO = repository.findById(c.getIdClient());
    }else {
        c.setIdClient(IDNum.generateRandomID());
        clientVO = c;
    }
    cancel.setVisible(persisted);
    binder.setBean(clientVO);
    setVisible(true);
    // Focus first name initially
    nameClient.focus();
    }

    public void setChangeHandler(ChangeHandler h) {
    changeHandler = h;
    }

}