package com.globalhitss.ui.interfaz.UI;


import com.globalhitss.ui.interfaz.UI.editor.EditorBuy;
import com.globalhitss.ui.interfaz.VO.BuyVO;
import com.globalhitss.ui.interfaz.service.BuyService;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Route("crud/buy")
public class BuyUI extends UIGeneral<BuyVO> {

    private BuyService repo;
    private EditorBuy editorBuy;

    @Autowired
    public BuyUI(BuyService repo,EditorBuy editorBuy) {
        super();
        this.repo = repo;
        this.editorBuy = editorBuy;
        grid = new Grid<>(BuyVO.class);
        add(actions, grid, editorBuy);
        grid.setHeight("300px");
        grid.setColumns("idBuy", "clientId", "productId","date_buy");
        grid.getColumnByKey("idBuy").setWidth("50px").setFlexGrow(0);
        grid.asSingleSelect().addValueChangeListener(e -> {
            editorBuy.editBuy(e.getValue());
        });
        this.editorBuy.setChangeHandler(()->{
            editorBuy.setVisible(false);
            listContent();
        });

        addNewBtn.addClickListener(e -> editorBuy.editBuy(new BuyVO(1,1,new Date())));

        listContent();
    }

    @Override
    void listContent() {
            List<BuyVO> list = repo.getAllBuys();
            grid.setItems(list);
    }
}
