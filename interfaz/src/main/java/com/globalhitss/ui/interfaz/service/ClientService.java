package com.globalhitss.ui.interfaz.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.globalhitss.ui.interfaz.VO.ClientVO;
import com.globalhitss.ui.interfaz.utils.Route;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.*;

@Repository
public class ClientService extends SuperService<ClientVO> {

    public ClientService() {
    }

    public List<ClientVO> getAllClients(){
        ObjectMapper mapper = new ObjectMapper();
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/json"));
        messageConverter.setObjectMapper(mapper);
        String uri = Route.url_backend.getURL()+"/clients";
        ResponseEntity<PagedResources<ClientVO>> responseEntity =
                new RestTemplate(Collections.singletonList(messageConverter))
                        .exchange(
                                uri,
                                HttpMethod.GET,
                                HttpEntity.EMPTY,
                                new ParameterizedTypeReference<PagedResources<ClientVO>>() {}, uri
                        );
        PagedResources<ClientVO> clients = responseEntity.getBody();
        assert clients != null;
        ArrayList<ClientVO> listClients = new ArrayList<>();
        for (ClientVO vo:clients ){
            listClients.add(vo);
        }
        return listClients;
    }

    @Override
    public ClientVO findById(Integer idClient){
        final String uri = Route.url_backend.getURL()+"/clients/{id}";
        Map<String, String> params = new HashMap<>();
        params.put("id", idClient+"");
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(uri, ClientVO.class, params);
    }

    public void delete(ClientVO clientVO) {
        setPath("/clients/{id}");
        delete(String.valueOf(clientVO.getIdClient()));
    }
    public void save(ClientVO clientVO){
        final String uri = Route.url_backend.getURL()+"/clients";
        RestTemplate restTemplate = new RestTemplate();
        ClientVO result = restTemplate.postForObject( uri, clientVO, ClientVO.class);
        System.out.println(result);
    }
}
