package com.globalhitss.ui.interfaz.utils;

public enum Route {
    url_backend("http://localhost:8081","Root API");
    String URL;
    String description;

    Route(String URL, String description) {
        this.URL = URL;
        this.description = description;
    }

    public String getURL() {
        return URL;
    }

    public String getDescription() {
        return description;
    }
}
