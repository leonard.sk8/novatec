package com.globalhitss.ui.interfaz.UI.editor;

import com.globalhitss.ui.interfaz.VO.ProductVO;
import com.globalhitss.ui.interfaz.service.ProductService;
import com.globalhitss.ui.interfaz.utils.IDNum;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class EditorProduct extends VerticalLayout implements KeyNotifier {

    private ProductService repository;

    private ProductVO productVO;

    /* Fields to edit properties in Customer entity */
    TextField nameProduct = new TextField("Name Product");
    TextField detailProduct = new TextField("Detail Product");
    TextField priceId = new TextField("Price Id");


    Button save = new Button("Save", VaadinIcon.CHECK.create());
    Button cancel = new Button("Cancel");
    Button delete = new Button("Delete", VaadinIcon.TRASH.create());
    HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

    Binder<ProductVO> binder = new Binder<>(ProductVO.class);
    private ChangeHandler changeHandler;


    @Autowired
    public EditorProduct(ProductService repository) {
    this.repository = repository;
    add(nameProduct,detailProduct,priceId , actions);

        binder.forField(priceId).withConverter(new StringToIntegerConverter("must be converted"))
                .bind(ProductVO::getPriceId, ProductVO::setPriceId);
        binder.forField(detailProduct).bind(ProductVO::getDetailProduct, ProductVO::setDetailProduct);
        binder.forField(nameProduct).bind(ProductVO::getNameProduct, ProductVO::setNameProduct);



    setSpacing(true);
    save.getElement().getThemeList().add("primary");
    delete.getElement().getThemeList().add("error");
    addKeyPressListener(Key.ENTER, e -> save());

    save.addClickListener(e -> save());
    delete.addClickListener(e -> delete());
    cancel.addClickListener(e -> editProduct(productVO));
    setVisible(false);
    }

    void delete() {
        repository.deleteProduct(productVO);
        changeHandler.onChange();
    }

    void save() {
        repository.save(productVO);
        changeHandler.onChange();
    }

    public interface ChangeHandler {
    void onChange();
    }

    public final void editProduct(ProductVO c) {
    if (c == null) {
    setVisible(false);
    return;
    }
    final boolean persisted = c.getIdProduct() != null;
    if (persisted) {
        productVO = repository.findById(c.getIdProduct());
    }else {
        c.setIdProduct(IDNum.generateRandomID());
    productVO = c;
    }
    cancel.setVisible(persisted);

    binder.setBean(productVO);
    setVisible(true);
    nameProduct.focus();
    }

    public void setChangeHandler(ChangeHandler h) {
    changeHandler = h;
    }

}