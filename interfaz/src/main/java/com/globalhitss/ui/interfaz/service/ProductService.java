package com.globalhitss.ui.interfaz.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.globalhitss.ui.interfaz.VO.PriceVO;
import com.globalhitss.ui.interfaz.VO.ProductVO;
import com.globalhitss.ui.interfaz.utils.Route;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Repository
public class ProductService  extends SuperService<ProductVO>{


    public List<ProductVO> getAllProducts() {
        ObjectMapper mapper = new ObjectMapper();
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/json"));
        messageConverter.setObjectMapper(mapper);
        String uri = Route.url_backend.getURL()+"/products";
        ResponseEntity<PagedResources<ProductVO>> responseEntity =
                new RestTemplate(Collections.singletonList(messageConverter))
                        .exchange(
                                uri,
                                HttpMethod.GET,
                                HttpEntity.EMPTY,
                                new ParameterizedTypeReference<PagedResources<ProductVO>>() {}, uri
                        );
        PagedResources<ProductVO> products = responseEntity.getBody();
        assert products != null;
            ArrayList<ProductVO> listProducts = new ArrayList<>();
        for (ProductVO vo:products ){
            listProducts.add(vo);
        }
        return listProducts;
    }

    public void deleteProduct(ProductVO obj) {
        setPath("/products/{id}");
        delete(String.valueOf(obj.getIdProduct()));
    }

    @Override
    public void save(ProductVO obj) {
        final String uri = Route.url_backend.getURL()+"/products";
        RestTemplate restTemplate = new RestTemplate();
        ProductVO result = restTemplate.postForObject( uri, obj, ProductVO.class);
        System.out.println(result);
    }

    @Override
    public ProductVO findById(Integer idProduct) {
        final String uri = Route.url_backend.getURL()+"/products/{id}";
        Map<String, String> params = new HashMap<>();
        params.put("id", idProduct+"");
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(uri, ProductVO.class, params);
    }
}
