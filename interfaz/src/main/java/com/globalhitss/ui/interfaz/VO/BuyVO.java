package com.globalhitss.ui.interfaz.VO;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuyVO {

    private Integer idBuy;

    private Integer clientId;

    private Integer productId;

    private Date date_buy;

    public BuyVO() {
    }


    public BuyVO(Integer clientId, Integer productId, Date date_buy) {
        this.clientId = clientId;
        this.productId = productId;
        this.date_buy = date_buy;
    }

    public BuyVO(Integer idBuy, Integer clientId, Integer productId, Date date_buy) {
        this.idBuy = idBuy;
        this.clientId = clientId;
        this.productId = productId;
        this.date_buy = date_buy;
    }

    public Integer getIdBuy() {
        return idBuy;
    }

    public void setIdBuy(Integer idBuy) {
        this.idBuy = idBuy;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Date getDate_buy() {
        return date_buy;
    }

    public void setDate_buy(Date date_buy) {
        this.date_buy = date_buy;
    }
}
