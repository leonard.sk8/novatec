package com.globalhitss.ui.interfaz.UI;

import com.globalhitss.ui.interfaz.VO.ClientVO;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

abstract class UIGeneral<T>  extends VerticalLayout {
    Grid<T> grid;
    Button addNewBtn;
    Button mainMenu;
    HorizontalLayout actions;


    public UIGeneral() {
        addNewBtn = new Button("New", VaadinIcon.PLUS.create());
        mainMenu = new Button("Main Menu");
        mainMenu.addClickListener(e->mainMenu.getUI().ifPresent(ui->ui.navigate("main")));
        actions = new HorizontalLayout(addNewBtn,mainMenu);
    }

    abstract void listContent();
}
