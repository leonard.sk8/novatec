package com.globalhitss.ui.interfaz.service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.globalhitss.ui.interfaz.VO.ClientVO;
import com.globalhitss.ui.interfaz.VO.PriceVO;
import com.globalhitss.ui.interfaz.utils.Route;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.*;


@Repository
public class PriceService extends SuperService<PriceVO> {


    public List<PriceVO> getAllPrices() {
        ObjectMapper mapper = new ObjectMapper();
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/json"));
        messageConverter.setObjectMapper(mapper);
        String uri = Route.url_backend.getURL()+"prices";
        ResponseEntity<PagedResources<PriceVO>> responseEntity =
                new RestTemplate(Collections.singletonList(messageConverter))
                        .exchange(
                                uri,
                                HttpMethod.GET,
                                HttpEntity.EMPTY,
                                new ParameterizedTypeReference<PagedResources<PriceVO>>() {}, uri
                        );
        PagedResources<PriceVO> pricesResources = responseEntity.getBody();
        assert pricesResources != null;
        ArrayList<PriceVO> listPrices = new ArrayList<>();
        for (PriceVO vo:pricesResources){
            listPrices.add(vo);
        }
        return listPrices;
    }

    public void deletePrice(PriceVO price) {
        setPath("/prices/{id}");
        delete(String.valueOf(price.getIdPrice()));
    }

    @Override
    public void save(PriceVO price) {
        final String uri = Route.url_backend.getURL()+"/prices";
        RestTemplate restTemplate = new RestTemplate();
        PriceVO result = restTemplate.postForObject( uri, price, PriceVO.class);
        System.out.println(result);
    }

    @Override
    public PriceVO findById(Integer idPrice) {
        final String uri = Route.url_backend.getURL()+"/prices/{id}";
        Map<String, String> params = new HashMap<>();
        params.put("id", idPrice+"");
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(uri, PriceVO.class, params);
    }
}
