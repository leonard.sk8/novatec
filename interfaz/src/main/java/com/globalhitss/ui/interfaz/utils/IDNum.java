package com.globalhitss.ui.interfaz.utils;

import java.util.Random;

public class IDNum {
     private static final Integer upperRange=10000;

    public static Integer generateRandomID(){
        Random random = new Random();
        return random.nextInt(upperRange);

    }
}
