package com.globalhitss.ui.interfaz.VO;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductVO {

    private Integer idProduct;
    private String nameProduct;
    private String detailProduct;
    private Integer priceId;

    public ProductVO() {
    }

    public ProductVO(String nameProduct, String detailProduct, Integer priceId) {
        this.nameProduct = nameProduct;
        this.detailProduct = detailProduct;
        this.priceId = priceId;
    }

    public ProductVO(Integer idProduct, String nameProduct, String detailProduct, Integer priceId) {
        this.idProduct = idProduct;
        this.nameProduct = nameProduct;
        this.detailProduct = detailProduct;
        this.priceId = priceId;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getDetailProduct() {
        return detailProduct;
    }

    public void setDetailProduct(String detailProduct) {
        this.detailProduct = detailProduct;
    }

    public Integer getPriceId() {
        return priceId;
    }

    public void setPriceId(Integer priceId) {
        this.priceId = priceId;
    }
}
