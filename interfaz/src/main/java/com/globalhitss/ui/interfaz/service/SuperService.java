package com.globalhitss.ui.interfaz.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.globalhitss.ui.interfaz.VO.PriceVO;
import com.globalhitss.ui.interfaz.utils.Route;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Repository
public abstract class SuperService<T>  {

    private String path;

    public String getPath() {
        return path;
    }

    public SuperService() {
    }

    public void setPath(String path) {
        this.path = path;
    }

    public PagedResources<T> getAll(){
        ObjectMapper mapper = new ObjectMapper();
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        messageConverter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/json"));
        messageConverter.setObjectMapper(mapper);
        String uri = Route.url_backend.getURL()+path;
        ResponseEntity<PagedResources<T>> responseEntity =
                new RestTemplate(Collections.singletonList(messageConverter))
                        .exchange(
                                uri,
                                HttpMethod.GET,
                                HttpEntity.EMPTY,
                                new ParameterizedTypeReference<PagedResources<T>>() {}, uri
                        );
        return responseEntity.getBody();
    }
    public void delete(String id){
        final String uri = Route.url_backend.getURL()+path;
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(uri,params);
    }
    public abstract void save(T obj);
    public abstract T findById(Integer idprice);


}
