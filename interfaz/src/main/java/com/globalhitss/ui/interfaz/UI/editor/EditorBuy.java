package com.globalhitss.ui.interfaz.UI.editor;

import com.globalhitss.ui.interfaz.VO.BuyVO;
import com.globalhitss.ui.interfaz.VO.ClientVO;
import com.globalhitss.ui.interfaz.VO.ProductVO;
import com.globalhitss.ui.interfaz.service.BuyService;
import com.globalhitss.ui.interfaz.service.ClientService;
import com.globalhitss.ui.interfaz.service.ProductService;
import com.globalhitss.ui.interfaz.utils.IDNum;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.LocalDateToDateConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;


@SpringComponent
@UIScope
public class EditorBuy extends VerticalLayout implements KeyNotifier {

    private BuyService repository;
    private ProductService repositoryProduct;
    private ClientService repositoryClient;

    private BuyVO buyVO;

    ComboBox<Integer> clientComboBox = new ComboBox<>("Seleccione Cliente");
    ComboBox<Integer> productComboBox = new ComboBox<>("Seleccione Producto");
    DatePicker datePicker = new DatePicker("Seleccione Fecha");

    Button save = new Button("Save", VaadinIcon.CHECK.create());
    Button cancel = new Button("Cancel");
    Button delete = new Button("Delete", VaadinIcon.TRASH.create());
    HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

    Binder<BuyVO> binder = new Binder<>(BuyVO.class);
    private ChangeHandler changeHandler;

    public EditorBuy(BuyService repository) {
        this.repository=repository;
        repositoryClient = new ClientService();
        repositoryProduct = new ProductService();
    fillClienteComboBox();
    fillProductComboBox();
    add(clientComboBox, productComboBox,datePicker, actions);
    binder.forField(clientComboBox).bind(BuyVO::getClientId, BuyVO::setClientId);
    binder.forField(productComboBox).bind(BuyVO::getProductId, BuyVO::setProductId);
    binder.forField(datePicker)
            .withConverter(new LocalDateToDateConverter(ZoneId.systemDefault()))
    .bind(BuyVO::getDate_buy, BuyVO::setDate_buy);
    setSpacing(true);
    save.getElement().getThemeList().add("primary");
    delete.getElement().getThemeList().add("error");
    addKeyPressListener(Key.ENTER, e -> save());

    save.addClickListener(e -> save());
    delete.addClickListener(e -> delete());
    cancel.addClickListener(e -> editBuy(null));
    setVisible(false);
    }

    private void fillClienteComboBox() {
        List<ClientVO> list = repositoryClient.getAllClients();
        List<Integer> listString = new ArrayList<>();
        for (ClientVO c:list){
            listString.add(c.getIdClient());
        }
        this.clientComboBox.setItems(listString);

    }
    private void fillProductComboBox() {
        List<ProductVO> list = repositoryProduct.getAllProducts();
        List<Integer> listString = new ArrayList<>();
        for (ProductVO p:list){
            listString.add(p.getIdProduct());
        }
        this.productComboBox.setItems(listString);
    }

    private void delete() {
        repository.delete(buyVO);
        changeHandler.onChange();
    }

    private void save() {
        repository.save(buyVO);
        changeHandler.onChange();
    }

    public interface ChangeHandler {
    void onChange();
    }

    public final void editBuy(BuyVO c) {
        if (c == null) {
        setVisible(false);
        return;
     }
    final boolean persisted = c.getIdBuy() != null;
     if (persisted) {
         buyVO = repository.findById(c.getIdBuy());
     }else {
         c.setIdBuy(IDNum.generateRandomID());
         buyVO = c;
     }
        cancel.setVisible(persisted);
        binder.setBean(buyVO);
        setVisible(true);
        clientComboBox.focus();
    }
    public void setChangeHandler(ChangeHandler h) {
    changeHandler = h;
    }

}