create database globalhitss;
use globalhitss;
CREATE TABLE clientVO (
  id_client INT NOT NULL,
  name_client VARCHAR(90) NOT NULL,
  last_name_client VARCHAR(90) NOT NULL,
  PRIMARY KEY (id_client)
);

select * from clientVO;

CREATE TABLE price (
  idprice integer NOT NULL IDENTITY,
  price float NOT NULL,
  PRIMARY KEY (idprice)
);


CREATE TABLE  product (
  idproduct INT NOT NULL IDENTITY,
  name_product VARCHAR(45) NOT NULL,
  detail_product VARCHAR(45) NULL,
  price_idprice INT NOT NULL,
  PRIMARY KEY (idproduct),
    FOREIGN KEY (price_idprice)
    REFERENCES price(idprice)
    );
CREATE TABLE  buy (
  idbuy INT NOT NULL IDENTITY,
  client_id_client INT NOT NULL,
  product_idproduct INT NOT NULL,
  date_buy date,
  PRIMARY KEY (idbuy),
  FOREIGN KEY (client_id_client)
REFERENCES clientVO (id_client),
    FOREIGN KEY (product_idproduct)
    REFERENCES product(idproduct)
 );